
![header_image_talker](imgs/readme_header.jpg?raw=true "Talker")
# Arquivos referente a palestras e minicurso


### Javascript
* [todo] IFAL - SPA Full Javascript e Automação de Processos.

### ReactJS:
* [todo] ReactJS - FrontIn Maceió

### Git:

* 1ª Mostra IFAL -
[Git - Gerenciamento de projetos e versionamento semântico](https://gitlab.com/djanilson-alves/talks/tree/master/primeira_mostra_ifal)

* Oxe Hacker Club -
[Git - Gerenciamento de projetos e versionamento semântico](https://gitlab.com/djanilson-alves/talks/tree/master/primeira_mostra_ifal)
